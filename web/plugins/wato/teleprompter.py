#!/usr/bin/env python
# -*- encoding: utf-8; py-indent-offset: 4 -*-

# This is free software;  you can redistribute it and/or modify it
# under the  terms of the  GNU General Public License  as published by
# the Free Software Foundation in version 2.  check_mk is  distributed
# in the hope that it will be useful, but WITHOUT ANY WARRANTY;  with-
# out even the implied warranty of  MERCHANTABILITY  or  FITNESS FOR A
# PARTICULAR PURPOSE. See the  GNU General Public License for more de-
# tails. You should have  received  a copy of the  GNU  General Public
# License along with GNU Make; see the file  COPYING.  If  not,  write
# to the Free Software Foundation, Inc., 51 Franklin St,  Fifth Floor,
# Boston, MA 02110-1301 USA.

from cmk.gui.valuespec import (
    Age,
    CascadingDropdown,
    Dictionary,
    DropdownChoice,
    EmailAddress,
    FixedValue,
    HTTPUrl,
    IPv4Address,
    ListChoice,
    ListOfStrings,
    Password,
    TextAreaUnicode,
    TextAscii,
    TextUnicode,
    Transform,
    Tuple,
)

from cmk.gui.plugins.wato import (
    notification_parameter_registry,
    NotificationParameter,
)

@notification_parameter_registry.register
class NotificationParameterteleprompter(NotificationParameter):
    @property
    def ident(self):
        return "teleprompter"

    @property
    def spec(self):
        return Dictionary(
            title=_("Create notification with the following parameters"),
            elements=[
                ("text", TextUnicode(
                    title=_("Textfield"),
                    help=_("Fill in the text you want to go into the notification"),
                    size=11,
                    allow_empty=True,
                )),
                ("dropdown",
                 DropdownChoice(
                     choices = [
                         ('1',_('First')),
                         ('2',_('Second')),
                         ('3',_('Third')),
                     ],
                     title=_("Seriousness"),
                     help=_("Dropdown field, choose the one to go into the notification. "
                            "Default value is Second"),
                     default = "2",
                 )),
                ("second_dropdown",
                 DropdownChoice(
                     choices = [
                         ('S1',_('Second_one')),
                         ('S2',_('Second_two')),
                         ('S3',_('Second_three')),
                     ],
                     title=_(""),
                     help=_("Dropdown field, choose the one to go into the notification. "
                            "Default value is Second_three"),
                     default = "S3",
                 )),
            ],
        )
