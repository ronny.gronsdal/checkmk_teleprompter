# teleprompter

## Description

This is a fork of the foobar teleprompter from the CheckMK documentation
The notification is the same, but I have added a script to include parameters set via Wato.

## Installation

Just copy the two folders, web and notifications, directly to /omd/sites/(yoursite)/local/share/check_mk/. Keep the folder structure.

## Usage

Go to setup - notifications in CheckMK, create a new notification rule or edit an existing one.
Under plugins choose the plugin teleprompter. You will now have three new choices under parameters. Theese are the same you will find in the teleprompter.py file under web/plugins/wato.

Test it using a fake check, and open the teleprompter output file where you will find all the NOTIFY\_ fields.

## Contributing

Feel free to use it as you wish.

## License

Theese scripts are provided as is, and are ment for testing/development purposes. Do not use in production environment. See the license file.
